
# Make sure the base images are updated
docker pull frappe/frappe-nginx:version-13
docker pull frappe/erpnext-worker:version-13

docker build --no-cache --build-arg GIT_BRANCH=version-13 -t nextoffice-worker -f build/nextoffice-worker/Dockerfile .
docker build --no-cache --build-arg GIT_BRANCH=version-13 -t nextoffice-nginx -f build/nextoffice-nginx/Dockerfile .


docker login registry.gitlab.com

docker tag nextoffice-worker registry.gitlab.com/sandah/frappe_docker/nextoffice-worker:version-13
docker push registry.gitlab.com/sandah/frappe_docker/nextoffice-worker:version-13
docker tag nextoffice-nginx registry.gitlab.com/sandah/frappe_docker/nextoffice-nginx:version-13
docker push registry.gitlab.com/sandah/frappe_docker/nextoffice-nginx:version-13